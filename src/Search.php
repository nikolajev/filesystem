<?php

namespace Nikolajev\Filesystem;

class Search
{
    public static function findFileFromParentDirs(string $fullFilePath, string $searchedFileTitle)
    {
        $exploded = explode(DIRECTORY_SEPARATOR, $fullFilePath);

        array_pop($exploded);

        $fileFound = false;

        while (!$fileFound) {
            $searchedFilePath = implode(DIRECTORY_SEPARATOR, $exploded) . DIRECTORY_SEPARATOR . $searchedFileTitle;
            $fileFound = file_exists($searchedFilePath);
            array_pop($exploded);
        }

        return $searchedFilePath;
    }
}