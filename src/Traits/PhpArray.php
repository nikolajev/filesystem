<?php

namespace Nikolajev\Filesystem\Traits;

use Nikolajev\Filesystem\File\FilePhpArray;

trait PhpArray
{
    // !filter(), !walk(), !map(), !unset([], true), !save() [var_export], ->values()->first(), ->keys()->first(), last()
    // ?append(), ?prepend(), merge(), rewriteMerge(), mergeRecursive()
    // !add()
    // slice()
    // unsetNullValues()
    private function _unset(array &$keys, bool $unsetEmptyOnly = true)
    {
        $result = &$this->data;
        $last = array_pop($keys);

        $references = [];

        foreach ($keys as $key) {
            $result = &$result[$key];
        }

        if ($unsetEmptyOnly && !empty($result[$last])) {
            return;
        }

        unset($result[$last]);
    }

    public function unset(string|array $keyOrKeys, bool $unsetEmptyParents = false): self
    {
        $keys = $keyOrKeys;

        if (is_string($keyOrKeys)) {
            $keys = [$keyOrKeys];
        }

        $this->_unset($keys, false);

        if (!$unsetEmptyParents) {
            return $this;
        }

        // Unset all parents if they are empty
        while (count($keys) > 0) {
            $this->_unset($keys);
        }

        return $this;
    }

    public function var_export(bool $squareBrackets = true)
    {
        if (!$squareBrackets) {
            return var_export($this->data, true);
        }

        // @link https://gist.github.com/Bogdaan/ffa287f77568fcbb4cffa0082e954022
        $export = var_export($this->data, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [NULL, ']$1', ' => ['], $array);
        $export = join(PHP_EOL, array_filter(["["] + $array));

        return $export;
    }

    private function referenceCallback(callable $callback, string|array $keyOrKeys = null)
    {
        if ($keyOrKeys === null) {
            $destination = &$this->data;
        } elseif (is_string($keyOrKeys)) {
            $destination = &$this->data[$keyOrKeys];
        } else {
            $destination = &$this->data;
            foreach ($keyOrKeys as $key) {
                $destination = &$destination[$key];
            }
        }

       $callback($destination);
    }

    public function add(mixed $value, string|array $keyOrKeys = null): self
    {
        $this->referenceCallback(function(&$destination) use ($value){
            $destination = $value;
        }, $keyOrKeys);

        return $this;
    }

    public function map(callable $callback, string|array $keyOrKeys = null): self
    {
        $this->referenceCallback(function(&$destination) use ($callback){
            $destination = array_map($callback, $destination);
        }, $keyOrKeys);

        return $this;
    }

    public function filter(callable $callback, string|array $keyOrKeys = null, int $mode = 0): self
    {
        $this->referenceCallback(function(&$destination) use ($callback, $mode){
            $destination = array_filter($destination, $callback, $mode);
        }, $keyOrKeys);

        return $this;
    }

    public function walk(callable $callback, string|array $keyOrKeys = null, mixed $arg = null): self
    {
        $this->referenceCallback(function(&$destination) use ($callback, $arg){
            $updateKeys = [];

            $call = function (&$value, $key) use (&$destination, $callback, &$updateKeys) {
                $result = $callback($value, $key);

                if ($result === FilePhpArray::WALK__UNSET) {
                    unset($destination[$key]);
                } elseif (is_array($result) && $result[0] === FilePhpArray::WALK__UPDATE_KEY) {
                    $updateKeys[$key] = $result[1];
                }
            };

            $success = array_walk($destination, $call, $arg);

            if (!$success) {
                trace();
                failure('array_walk() failed');
            }

            foreach ($updateKeys as $keyBefore => $keyAfter) {
                $destination[$keyAfter] = $destination[$keyBefore];
                unset($destination[$keyBefore]);
            }
        }, $keyOrKeys);

        return $this;
    }
}