<?php

namespace Nikolajev\Filesystem\File;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\Filesystem\Interfaces\FileInterface;

class CsvFile extends FileBase implements FileInterface
{
    protected array $data = [];

    private string $delimiter;

    public function __construct(string $filePath, string $data = null, string $delimiter = null)
    {
        parent::__construct($filePath);

        $this->delimiter = $delimiter ?? $GLOBALS['nikolajev']['filesystem']['csv-file']['delimiter'] ?? ',';

        if (file_exists($this->getFullPath())) {
            $this->setDataFromFile();
        }

        if ($data !== null) {
            $this->setDataFromString($data);
        }
    }


    public function setDataFromFile()
    {
        $lines = file($this->getFullPath());

        foreach ($lines as $key => $value) {
            $this->data[] = str_getcsv($value, $this->delimiter);
        }
    }

    public function setDataFromString(string $string)
    {
        foreach (explode(PHP_EOL, $string) as $key => $value) {
            $this->data[] = str_getcsv($value, $this->delimiter);
        }
    }


    public function toArray()
    {
        return $this->data;
    }

    public function toArrayObject(): ArrayObject
    {
        // @todo throw exception if data is empty!
        return (new ArrayObject($this->data))->setModifications($this->modificationHistory)->addModification($this);
    }
}