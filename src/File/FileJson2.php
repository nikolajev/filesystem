<?php

namespace Nikolajev\Filesystem\File;

use Nikolajev\Filesystem\Search;

// @todo Use interface with save()
class FileJson2 extends FileBase
{
    protected string $data;

    // isJson()
    public function __construct(string $filePath, string $data = null, bool $rewrite = false)
    {
        //$type = 'json';

        $this->setType('json')->setExtension('json');

        parent::__construct($filePath);

        // @todo Extra here
        //$this->validateType($type);

        // @todo DRY
        if ($data === null) {
            if (!file_exists($this->getFullPath())) {
                // @todo exit on failure only if set so globally (avoiding 'exit' is required for testing purposes)
                failure("File '{$this->getFullPath()}' does not exist");//exit;
            }
            return $this->data = file_get_contents($this->getFullPath());
        }

        // @todo DRY
        if(!$rewrite && file_exists($this->getFullPath())){
            failure("File '{$this->getFullPath()}' already exists");//exit;
        }

        $this->data = $data;
    }

    public function toArray()
    {
        $data = empty($this->data) ? [] : json_decode($this->data, true);

        return new FilePhpArray(
        //$this->getFullPath('php'),
            $this->filePath,
            $data
        );
    }

    public function setData(string $data): self
    {
        $this->data = $data;
        return $this;
    }

    /* @todo
     * public function toObject()
     * {
     * return json_decode(file_get_contents($this->getFullPath()));
     * }
     */

    public function save()
    {
        file_put_contents($this->getFullPath(), $this->data);
    }
}