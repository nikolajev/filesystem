<?php

namespace Nikolajev\Filesystem\File;

use Nikolajev\Filesystem\Interfaces\FileInterface;
use Nikolajev\Filesystem\Search;

abstract class FileBase2 implements FileInterface
{
    protected const DS = DIRECTORY_SEPARATOR;

    protected string $filePath;

    private string $type = 'txt';
    
    private string $extension = 'txt';

    private string $callerFile;

    private string $composerDir;

    public function __construct(string $filePath)
    {
        $this->callerFile = debug_backtrace()[2]['file'];

        $this->composerDir = dirname(Search::findFileFromParentDirs($this->callerFile, 'composer.json')) . self::DS;

        $this->filePath = $this->prepareFilePath($filePath);

        //$extension = $this->parseExtension($filePath);
        // @todo Always should be 'json' ???
        //$this->type = $extension === null ? $this->getType() : $extension;
    }

    private function prepareFilePath(string $filePath)
    {
        $filePath = preg_replace('/\b.' . $this->getExtension() . '$/', '', $filePath);
        
        if (str_starts_with($filePath, "/")) {
            return $filePath;
        }

        if (str_starts_with($filePath, "./")) {
            return dirname($this->callerFile) . self::DS . substr($filePath, 2);
        }

        return $this->composerDir . $filePath;
    }

    // @todo 
    public function getFullPath(string $replaceExtensionWith = null)
    {
        return $this->filePath . "." . $this->getExtension();
        /*
        $extension = $this->parseExtension($this->filePath);

        if ($replaceExtensionWith === null) {
            if ($extension === null || $extension !== $this->getType()) {
                return $this->filePath . "." . $this->getType();
            }

            return $this->filePath;
        }

        if ($extension === null) {
            return $this->filePath . ".$replaceExtensionWith";
        }

        return preg_replace('/\b' . $extension . '$/', $replaceExtensionWith, $this->filePath);
        */
    }

    private function parseExtension(string $filePath)
    {
        if (!str_contains(basename($filePath), ".")) {
            return null;
        }

        $exploded = explode(".", basename($filePath));
        $extension = array_pop($exploded);

        return empty($extension) ? null : $extension;
    }

    public function getType()
    {
        return $this->type;
    }

    protected function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension(string $extension): self
    {
        $this->extension = $extension;
        return $this;
    }
    
    

    protected function validateType(string $type)
    {
        // @todo Update
        if ($this->getType() !== $type) {
            trigger_error("Parsed file type is not json", E_USER_WARNING);
            trace(4, 1);
        }
    }

    // @todo
    protected function setInitData(mixed $data)
    {
    }

    public function return()
    {
        return $this->data ?? null;
    }

    // @todo rename()
}