<?php

namespace Nikolajev\Filesystem\File;

use Nikolajev\Filesystem\Interfaces\FileInterface;
use Nikolajev\Filesystem\Search;

abstract class FileBase implements FileInterface
{
    protected const DS = DIRECTORY_SEPARATOR;

    protected string $filePath;

    private string $callerFile;

    private string $composerDir;

    protected array $modificationHistory = [];


    public function __construct(string $filePath)
    {
        $this->callerFile = debug_backtrace()[2]['file'];

        // @todo NB!!! No logics in naming. findFileDirPathFromParentDirs()
        $this->composerDir = dirname(Search::findFileFromParentDirs($this->callerFile, 'composer.json')) . self::DS;

        $this->filePath = $this->prepareFilePath($filePath);
    }

    // @todo string|array $data = null
    public function setData(array $data = []): self
    {
        $this->data = $data;

        return $this;
    }


    // @todo DRY (separate trait) + interface ?
    public function setModifications(array $modifications = []): self
    {
        $this->modificationHistory = $modifications;

        return $this;
    }

    // @todo DRY (separate trait) + interface ?
    public function addModification(object $object): self
    {
        $this->modificationHistory[] = $object;

        return $this;
    }


    private function prepareFilePath(string $filePath)
    {
        if (str_starts_with($filePath, "/")) {
            return $filePath;
        }

        if (str_starts_with($filePath, "./")) {
            return dirname($this->callerFile) . self::DS . substr($filePath, 2);
        }

        return $this->composerDir . $filePath;
    }

    public function getFullPath()
    {
        return $this->filePath;
    }
}