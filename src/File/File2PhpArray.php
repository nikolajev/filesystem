<?php

namespace Nikolajev\Filesystem\File;

use Nikolajev\Filesystem\Traits\PhpArray;

// @todo Use interface with save(), move(), rename(), etc.
class File2PhpArray extends FileBase
{
    use PhpArray;

    const WALK__UNSET = 'array_walk__unset';
    const WALK__UPDATE_KEY = 'array_walk__update_key';

    protected array $data;

    public function __construct(string $filePath, array $data = null, bool $rewrite = false)
    {
        parent::__construct($filePath);

        $this->setType('php-array');
        $this->setExtension('php');

        // @todo Set data process same way as in FileJson
        if ($data === null) {
            if (!file_exists($this->getFullPath())) {
                // @todo exit on failure only if set so globally (avoiding 'exit' is required for testing purposes)
                // Debugger::exitOnFailure(false) - defaults to true
                failure("File '{$this->getFullPath()}' does not exist");//exit;
            }
            return $this->data = require $this->getFullPath();
        }

        // @todo DRY
        if(!$rewrite && file_exists($this->getFullPath())){
            failure("File '{$this->getFullPath()}' already exists");//exit;
        }

        $this->data = $data === null ? [] : $data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    public function save(bool $squareBrackets = true)
    {
        file_put_contents($this->getFullPath(), "<?php\n\nreturn " . $this->var_export($squareBrackets) . ";");
    }

    // @todo Use FileJsonParams() instead of flags
    public function toJson(int $flags = null)
    {
        $flags = $flags ?? JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT;

        return new FileJson(
        //$this->getFullPath('json'),
            $this->filePath,
            json_encode($this->data, $flags)
        );
    }
}