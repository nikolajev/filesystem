<?php

namespace Nikolajev\Filesystem;

use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\File\CsvFile;
//use Nikolajev\Filesystem\File\FileJson;
//use Nikolajev\Filesystem\File\FilePhpArray;

class File
{
    /*
    public static function json(string $filePath, string $data = null)
    {
        return new FileJson($filePath, $data);
    }

    public static function array(string $filePath, array $data = null)
    {
        return new FilePhpArray($filePath, $data);
    }
*/
    public static function csv(string $filePath, string $data = null)
    {
        return new CsvFile($filePath, $data);
    }

    // @todo File::config()->csvDelimiter()
    public static function csvDelimiter(string $delimeter)
    {
        $GLOBALS['nikolajev']['filesystem']['csv-file']['delimiter'] = $delimeter;

        // @todo
        Data::array();
    }
}